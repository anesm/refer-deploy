# Refer

**Important Notes**
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) that uses react-scripts package, which makes it suciptable to bugs and deps conflicts.
Make sure your running the same npm version as in the project's. You can use [nvm](https://github.com/nvm-sh/nvm) as a version manager.
Please create an issue if you encoutered any sort of problems runnig the project.

## Getting Started

_Make sure that **node** and **npm** are both installed on your machine._

Installing

```
npm install
```

## Running the project locally

```
npm run start
```

Visit [http://localhost:3000](http://localhost:3000) to view the project in your browser.

## Directory Structure

```
.
├── src
│   ├── api
│   ├── assets
│   ├── components
│   ├── index.tsx
│   ├── styles
│   └── utils
```
