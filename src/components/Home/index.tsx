import React, { useEffect, useState, useRef } from 'react';
import { useParams } from 'react-router-dom';
import { useMediaQuery } from 'react-responsive'; // We'll need it for contents
import { isMobile } from 'react-device-detect'; // We'll need it to detect browsers and devices

import { getData } from '../../api';
import LeftCol from '../LeftCol';
import RightCol from '../RightCol';
import ImagesWrapper from '../Images';
// const _ID = 'sn1u7ai'

const Home = () => {
  const [data, setData] = useState();
  const [invalidRefer, setInvalidRefer] = useState(false);
  const [isExpanded, setIsExpanded] = useState(false);
  const { shortlink_id } = useParams();
  const collapsableRef = useRef();
  const homeRef = useRef();

  useEffect(() => {
    getData(shortlink_id)
      .then((data) => {
        if (!data) throw new Error('No Data');
        setData(data);
      })
      .catch((_err) => setInvalidRefer(true));
  }, []);

  const onClick = () => {
    homeRef.current.classList.toggle('expanded');
    return setIsExpanded(!isExpanded);
  };

  // When refer is invalid
  if (invalidRefer) {
    return <h1>NOT FOUND</h1>;
  }
  //TOFIX: remove the flickering caused by the following jsx block on invalidRefer.
  return (
    <div className="home-container" ref={homeRef}>
      <LeftCol isMobile={isMobile} isExpanded={isExpanded} data={data}>
        {/* prevents undefined to be passed to child on initial render */}
        {data && <ImagesWrapper data={data} />}
      </LeftCol>
      {/* collapsable */}
      <RightCol
        ref={collapsableRef}
        expand={onClick}
        isExpanded={isExpanded}
      ></RightCol>
    </div>
  );
};

export default Home;
