import React from 'react';
import _ from 'underscore';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import Home from './Home';
import Solicitation from './Solicitation';

const App = () => {
  return (
    <Router>
      <Routes>
        <Route path="/:shortlink_id" element={<Home />} />
        <Route path="/solicitation/:shortlink_id" element={<Solicitation />} />
      </Routes>
    </Router>
  );
};

export default App;
