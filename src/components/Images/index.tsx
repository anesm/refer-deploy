import React, { useEffect, useState, useRef } from 'react';

import {
  getThumbnails,
  getListOfFirstNames,
  getListOfLastNames,
  onImageError,
} from './_helpers';

type Thumbnails = {
  [key: string]: string;
};
const initURLs: Thumbnails = {
  sender: '',
  receiver: '',
  searchFor: '',
};

const ImagesWrapper = ({ data }: any) => {
  // =images' urls
  const [urls, setUrls] = useState(initURLs);
  const [firstNames, setFirstNames] = useState([]);
  const [lastNames, setLastNames] = useState([]);

  // =Refs
  const img1Ref = useRef();
  const img2Ref = useRef();
  const img3Ref = useRef();
  const REFS = {
    0: img1Ref,
    1: img2Ref,
    2: img3Ref,
  };

  useEffect((): void => {
    getThumbnails(data.result, setUrls);
    getListOfFirstNames(data.result, setFirstNames);
    getListOfLastNames(data.result, setLastNames);
  }, []);

  const renderImages = () => {
    return Object.keys(urls).map((user, i) => {
      return (
        <span className="image-wrapper" key={i} ref={REFS[i]} data-ref={`${i}`}>
          <img
            className="dynamic-image"
            src={urls[user]}
            // onLoad={(e) => console.log('loaded', firstNames)}
            onError={(e) => onImageError(e, REFS, [firstNames, lastNames])} // use callback so we don't trigger fn twice
          />
        </span>
      );
    });
  };

  return <div className="images-container">{renderImages()}</div>;
};

export default ImagesWrapper;
