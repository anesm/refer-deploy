import { betterPluck, importAll, removeInvalidChars } from '../../utils';

const _IMAGES = Object.entries(
  importAll(require.context('../../assets/bgs', false, /\.(png)$/))
);

const getThumbnails = (response: object, setter): void => {
  const thumbnailList = betterPluck(response, 'thumbnail_path');
  setter((prev) => ({
    ...prev,
    sender: thumbnailList[0],
    receiver: thumbnailList[1],
    searchFor: thumbnailList[2],
  }));
};

const getListOfFirstNames = (response: any, setter): Array<string> =>
  setter(betterPluck(response, 'first_name'));
const getListOfLastNames = (response: any, setter): Array<string> =>
  setter(betterPluck(response, 'last_name'));

const _getFirstLetters = (name: string): string =>
  removeInvalidChars(name).trim().charAt(0).toLowerCase();

const _getInitials = (list: string[], index: number) => {
  const firstLetter = _getFirstLetters(list[0][index]);
  const secondLetter = _getFirstLetters(list[1][index]);
  return {
    firstLetter,
    secondLetter,
    initials: firstLetter + secondLetter,
  };
};

const _getIndices = (firstLetter, secondLetter) => {
  const alphabet = 'abcdefghijklmnopqrstuvwxyz';
  const first_index = alphabet.indexOf(firstLetter);
  const second_index = alphabet.indexOf(secondLetter);
  return first_index + second_index;
};

const onImageError = (event, REFS, namesList) => {
  const { target } = event;
  const imageElem = target;
  const wrapper = target.parentElement;
  const ref = REFS[wrapper.dataset.ref];
  const id = wrapper.dataset.ref;
  const { initials, firstLetter, secondLetter } = _getInitials(namesList, id);
  let localImageRandomIndex =
    _getIndices(firstLetter, secondLetter) % _IMAGES.length;
  if (localImageRandomIndex < 0) localImageRandomIndex *= -1;
  const url = _IMAGES[localImageRandomIndex][1];
  imageElem.remove();
  ref.current.style.backgroundImage = `url(${url})`;
  const span = document.createElement('span');
  span.className = `title ${wrapper.dataset.ref}`;
  span.innerHTML = initials.toUpperCase(); // static
  ref.current.appendChild(span);
};

export { getThumbnails, getListOfFirstNames, getListOfLastNames, onImageError };
