import React from 'react';

import imgBottomRect from '../../assets/ellipses/Group 628173.png';
import imgUpperRect from '../../assets/ellipses/ellipse_1254.png';

const Network = () => {
  return (
    <div className="col-right__body__network__container">
      <div className="col-right__body__network__wrapper">
        {/* TODO: implement a pure CSS solution for better interactivity and micro-animations */}
        <span className="col-right__body__network__img-wrapper"></span>

        <div className="network__rectangle upper">
          <span className="network__rectangle__header">
            <span></span>
            REQUÊTE DU RESEAU
          </span>
          <div className="network__rectangle__body">
            <span className="network__rectangle__body__img">
              <img src={imgUpperRect} />
            </span>
            <div className="network__rectangle__body__text">
              <span className="network__rectangle__body__text__main">
                Salut, je cherche des
              </span>
              <div className="network__rectangle__body__text__animated-mask">
                <span className="network__rectangle__body__text__animated-container">
                  <span id="clients">clients!</span>
                  <span id="talents">talents!</span>
                  <span id="investors">investors!</span>
                  <span id="partners">partners</span>
                </span>
              </div>
            </div>
          </div>
          <button className="network__rectangle__button">
            <span></span>23 messages
          </button>
        </div>

        <div className="network__rectangle bottom">
          <span className="network__rectangle__header">
            <span></span>
            VOUS POUVEZ PRÉSENTER
          </span>
          <span className="network__rectangle__body">
            <div className="network__rectangle__body__images">
              <img src={imgBottomRect} />
            </div>
            <div className="network__rectangle__body__text">
              profils pertinents
            </div>
          </span>
          <button className="network__rectangle__button">Meet them</button>
        </div>
      </div>
    </div>
  );
};

export default Network;
