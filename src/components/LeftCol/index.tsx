import React from 'react';

const LeftCol: React.FC<HTMLElement> = ({ isMobile, isExpanded, children }) => {
  const renderH1Content = () => (isMobile ? 'Referred !' : 'Toc, toc');
  const renderPContents = () => {
    // if mobile return something else.
    return (
      <>
        <p className="col-left__p">
          Saluez votre nouveau contact dans la messagerie LinkedIn !
        </p>
      </>
    );
  };

  return (
    <div className="col-left__container">
      {isMobile ? (
        <div className="col-left__logo__container">
          <span className="col-left__logo"></span>
          <p className="col-left__logo-slogon">Your Networking Agent</p>
        </div>
      ) : null}

      <h1 className="col-left__h">{renderH1Content()}</h1>
      {renderPContents()}
      {children}
    </div>
  );
};

export default LeftCol;
