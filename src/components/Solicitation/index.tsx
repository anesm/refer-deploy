import React from 'react';
import { useParams } from 'react-router-dom';

import Left from './Left';
import Right from './Right';

const Solicitation = () => {
  console.log(useParams());
  return (
    <div className="home-container">
      <Left />
      <Right />
    </div>
  );
};

export default Solicitation;
