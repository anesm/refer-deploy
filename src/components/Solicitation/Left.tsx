import React from 'react';

import promptImage from '../../assets/placeholders/prompt_image.png';

const Left = () => {
  return (
    <div className="col-left__container sollicit">
      <div className="col-left__wrapper">
        {/* TODO: prompt's user name and image will come from api */}
        <div className="col-left__prompt">
          <div className="col-left__prompt__image__wrapper">
            <img className="col-left__prompt__image" src={promptImage} />
          </div>
          <div className="col-left__prompt__text">
            <span className="col-left__prompt__user-name">Arnaud</span> needs
            you!
          </div>
        </div>

        <div className="col-left__form-wrapper"></div>
      </div>
    </div>
  );
};

export default Left;
