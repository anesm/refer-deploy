import React from 'react';

import appleStore from '../../assets/apple_store.png';
import googleStore from '../../assets/google_play.png';

const StoreButton: React.FC<HTMLElement> = ({ store }) =>
  store === 'apple' ? (
    <button className="col-right__body__store-button apple">
      <img className="button-icon" src={appleStore} />
    </button>
  ) : (
    <button className="col-right__body__store-button google">
      <img className="button-icon" src={googleStore} />
    </button>
  );

export default StoreButton;
