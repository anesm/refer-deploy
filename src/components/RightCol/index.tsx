import React from 'react';
import { isMobile } from 'react-device-detect';

import Network from '../Network';
import backArrow from '../../assets/icons/back_arrow.svg';

// eslint-disable-next-line react/display-name
const RightCol = React.forwardRef<HTMLElement>((props, ref) => {
  // eslint-disable-next-line react/prop-types
  const { expand, isExpanded } = props;

  const renderPortion = () => (
    <div className="col-right__portion">
      {/* <h4>Become a social beast in no-time</h4> */}
      <h4>Découvrez le pouvoir de votre réseau avec Refer</h4>
      <h4></h4>
      <div className="portion__buttons">
        <button className="portion__btn-left">
          <span className="icon"></span>
          <span>Get the app</span>
        </button>
        <button className="portion__btn-right" onClick={expand}>
          <span>See more</span>
          <span className="icon"></span>
        </button>
      </div>
    </div>
  );

  const renderH1 = () => {
    if (isMobile) {
      return 'Ready to meet \n your next opportunity?';
    } else if (isExpanded && !isMobile) {
      return `Dévoilez l’immense pouvoir\ndu réseau !`;
    } else {
      return 'Découvrez le pouvoir de \n votre réseau avec Refer';
    }
  };

  const renderP = () => {
    if (isExpanded && isMobile) {
      return 'Activate your network to get a maximum \n of introductions. Make dozens of introductions \n in just a few clicks.';
    }
    if (isExpanded && !isMobile) {
      return 'Sollicitez votre réseau pour rencontrer la bonne personne, au \nbon moment.\n\nOu aidez vos contacts à trouver ce qu’ils cherchent en 1 clic !';
    } else {
      return null;
    }
  };

  return (
    <div className="col-right__container" ref={ref}>
      {isMobile && !isExpanded ? renderPortion() : null}

      <div className="col-right__wrapper">
        <div className="col-right__header">
          {isMobile ? (
            <button className="col-right__header__button" onClick={expand}>
              <img className="back-arrow" src={backArrow} />
              Got it!
            </button>
          ) : (
            <>
              <span className="col-right__logo"></span>
              <p className="col-right__header__p">Votre agent business.</p>
            </>
          )}
        </div>

        <div className="col-right__body">
          <h1 className="col-right__body__h">{renderH1()}</h1>
          {/* {isExpanded ? ( */}
          <p className="col-right__body__p">{renderP()}</p>
          {/* ) : null} */}
          <button className="col-right__body__btn">
            {isMobile ? `Download Now` : `Free download`}
          </button>
        </div>

        <button className="col-right__expand" onClick={expand}>
          Discover<span className="col-right__expand__arrow"></span>
        </button>

        {/* NETWORK */}
        <Network />
      </div>
    </div>
  );
});

export default RightCol;
