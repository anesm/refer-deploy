export const getData = async (param: string | undefined) => {
  try {
    const response = await fetch(
      `https://staging.refer.social:8000/messages/details/${param}`
    );
    if (!response.ok) {
      throw new Error('HTTP error');
    }
    const data = await response.json();
    return data;
  } catch (error) {
    throw new Error(error);
  }
};
