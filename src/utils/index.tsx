import _ from 'underscore';

const _importAll = (require) =>
  require.keys().reduce((prev, next) => {
    prev[next.replace('./', '')] = require(next);
    return prev;
  }, {});

const _betterPluck = (iterator, propertyName) => {
  return _.filter(_.pluck(iterator, propertyName), (item) => item);
};

const _removeInvalidChars = (name: string): string => {
  return name.replace(
    /([\u2700-\u27BF]|[\uE000-\uF8FF]|\uD83C[\uDC00-\uDFFF]|\uD83D[\uDC00-\uDFFF]|[\u2011-\u26FF]|\uD83E[\uDD10-\uDDFF])/g,
    ''
  );
};

export {
  _importAll as importAll,
  _betterPluck as betterPluck,
  _removeInvalidChars as removeInvalidChars,
};
